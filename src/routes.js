import React from 'react';
import { Router, Route, browserHistory } from 'react-router';

import LayersMap from './containers/LayersMap';


export default () => (
  <Router history={browserHistory}>
    <Route path='/' component={LayersMap} />
  </Router>
);
