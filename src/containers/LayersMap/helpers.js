export function compareMaker(latlng1 = {}, latlng2 = {}) {
  return latlng1.lat === latlng2.lat && latlng1.lng === latlng2.lng;
}


export function generateId() {
  // Правильно уникальный id будет генериться на сервере, это условность тестового задания
  return Math.ceil((Math.random() * (1000 - 10)) + 100);
}
