import { compose } from 'redux';
import { connect } from 'react-redux';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import LefletGeoMap from '../../components/LefletGeoMap';
import reducer,
{
  addMarker, deleteMarker, fetchLayersList,
  fetchLayerInfo, openModalAddLayer, addLayerToList,
  deleteLayerFromList, editLayerName, onSaveLayer,
} from './reducer';
import saga from './saga';


const mapStateToProps = (state) => {
  const activeId = state.get('layersMap').get('activeLayerId');
  const layersMarkers = state.get('layersMap').get('layersMarkers').toJS();
  return ({
    layersList: state.get('layersMap').get('layersList').toJS(),
    layerMarkers: layersMarkers[activeId],
    isOpenAddLayer: state.get('layersMap').get('isOpenAddLayer'),
    activeId,
  });
};


const withConnect = connect(mapStateToProps, {
  onAddMarker: addMarker,
  onDeleteMarker: deleteMarker,
  onFetchLayersList: fetchLayersList,
  onChangeLayer: fetchLayerInfo,
  addLayerToList,
  openModalAddLayer,
  onDeleteLayerFromList: deleteLayerFromList,
  onEditLayerName: editLayerName,
  onSaveLayer,
});

const withReducer = injectReducer({ key: 'layersMap', reducer });
const withSaga = injectSaga({ key: 'layersMap', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(LefletGeoMap);

