import { createSelector } from 'reselect';


export const selectLayersMap = state => state.get('layersMap');

const makeSelectLayersInfo = () => createSelector(
  selectLayersMap,
  substate => substate.toJS(),
);


export default makeSelectLayersInfo;
