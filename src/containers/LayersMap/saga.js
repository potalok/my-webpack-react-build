import { call, put, takeLatest, takeEvery, all } from 'redux-saga/effects';
import {
  fetchLayersList, fetchLayersListSuccess, fetchLayersListFail,
  fetchLayerInfo, addLayerToList as addLayerToListAction, addLayerToListSuccess,
  deleteLayerFromList as deleteLayerFromListAction, deleteLayerFromListSuccess,
  deleteLayerFromListFail, fetchLayerInfoSuccess, fetchLayerInfoFail,
  onSaveLayer as onSaveLayerAction, onSaveLayerSuccess, onSaveLayerFail,
} from './reducer';

const defaultData = {
  activeLayerId: 1,
  layersList: [
    { name: 'Москва', id: 1, latlng: { lat: 55.75086519256549, lng: 37.61816185787291 } },
    { name: 'Париж', id: 2, latlng: { lat: 48.851952544057184, lng: 2.3307869769280214 } },
    { name: 'Рим', id: 3, latlng: { lat: 41.91556321720713, lng: 12.471866051566627 } },
  ],
  layersMarkers: {},
};


// Важно, чтобы тествое задание работало визуально как нужно в соответсвии с требованиями без бекенда,
// но как будто с ним, тут допущено ряд условностей,
// загружаемые как бы от сервера данные на самом деле уже присутствуют в store, и нет обработки ошибок от сервера
// И часть кода это псевдокод который нужен только в контексте выполнения задания

const request = params => () => new Promise((resolve) => {
  resolve(params);
});


function* getLayersListRequest() {
  try {
    const response = yield call(request(defaultData));
    yield put(fetchLayersListSuccess(response));
  } catch (err) {
    yield put(fetchLayersListFail(err));
  }
}


function* getLayerInfoRequest() {
  try {
    yield put(fetchLayerInfoSuccess());
  } catch (err) {
    yield put(fetchLayerInfoFail(err));
  }
}

function* addLayerToListRequest({ payload }) {
  try {
    yield put(addLayerToListSuccess(payload));
  } catch (err) {
    yield put(fetchLayersListFail(err));
  }
}

function* deleteLayerFromListRequest({ payload } = {}) {
  try {
    yield put(deleteLayerFromListSuccess(payload));
  } catch (err) {
    yield put(deleteLayerFromListFail(err));
  }
}

function* onSaveChangeLayerRequest({ payload }) {
  try {
    yield call(request(payload));
    yield put(onSaveLayerSuccess());
  } catch (err) {
    yield put(onSaveLayerFail(err));
  }
}

function* onSaveChangeLayer() {
  yield takeEvery(onSaveLayerAction, onSaveChangeLayerRequest);
}

function* deleteLayerFromList() {
  yield takeEvery(deleteLayerFromListAction, deleteLayerFromListRequest);
}

function* addLayerToList() {
  yield takeEvery(addLayerToListAction, addLayerToListRequest);
}

function* getLayerInfo() {
  yield takeEvery(fetchLayerInfo, getLayerInfoRequest);
}

function* getLayersList() {
  yield takeLatest(fetchLayersList, getLayersListRequest);
}

export default function* defaultSaga() {
  yield all([
    onSaveChangeLayer(),
    deleteLayerFromList(),
    addLayerToList(),
    getLayersList(),
    getLayerInfo(),
  ]);
}
