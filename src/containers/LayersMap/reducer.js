import { createAction, createReducer } from 'redux-act';
import { fromJS } from 'immutable';
import { compareMaker, generateId } from './helpers';


const initialState = fromJS({
  isOpenAddLayer: false,
  activeLayerId: null,
  layersList: [],
  layersMarkers: {},
  error: null,
});

// Важно, чтобы тествое задание работало визуально как нужно в соответсвии с требованиями без бекенда,
// но как будто с ним, тут допущено ряд условностей,
// загружаемые как бы от сервера данные на самом деле уже присутствуют в store, и нет обработки ошибок от сервера
// И часть кода это псевдокод который нужен только в контексте выполнения задания

export const fetchLayersList = createAction('/LAYERS_MAP/FETCH_LAYERS_LIST');
export const fetchLayersListSuccess = createAction('/LAYERS_MAP/FETCH_LAYERS_LIST_SUCCESS');
export const fetchLayersListFail = createAction('/LAYERS_MAP/FETCH_LAYERS_LIST_FAIL');


export const fetchLayerInfo = createAction('/LAYERS_MAP/FETCH_LAYER_INFO');
export const fetchLayerInfoSuccess = createAction('/LAYERS_MAP/FETCH_LAYER_INFO_SUCCESS');
export const fetchLayerInfoFail = createAction('/LAYERS_MAP/FETCH_LAYER_INFO_FAIL');

export const openModalAddLayer = createAction('/LAYERS_MAP/OPEN_MODAL_ADD_LAYER');

export const addLayerToList = createAction('/LAYERS_MAP/ADD_LAYER_TO_LIST');
export const addLayerToListSuccess = createAction('/LAYERS_MAP/ADD_LAYER_TO_LIST_SUCCESS');
export const addLayerToListFail = createAction('/LAYERS_MAP/ADD_LAYER_TO_LIST_FAIL');

export const editLayerName = createAction('/LAYERS_MAP/EDIT_LAYER_NAME');

export const deleteLayerFromList = createAction('/LAYERS_MAP/DELETE_LAYERS_FROM_LIST');
export const deleteLayerFromListSuccess = createAction('/LAYERS_MAP/DELETE_LAYERS_FROM_LIST_SUCCESS');
export const deleteLayerFromListFail = createAction('/LAYERS_MAP/DELETE_LAYERS_FROM_LIST_FAIL');

export const onSaveLayer = createAction('/LAYERS_MAP/ON_SAVE_CHANGES');
export const onSaveLayerSuccess = createAction('/LAYERS_MAP/ON_SAVE_CHANGES_SUCCESS');
export const onSaveLayerFail = createAction('/LAYERS_MAP/ON_SAVE_CHANGES_FAIL');

export const addMarker = createAction('/LAYERS_MAP/ADD_MARKER');
export const deleteMarker = createAction('/LAYERS_MAP/DELETE_MARKER');


const handleOnSaveLayerSuccess = state => state;

const handleOnSaveLayerFail = (state, error) => state.set('error', error);

const handleOpenModalAddLayer = (state, status) => state.set('isOpenAddLayer', status);

const handleAddLayerToList = state => state.set('isOpenAddLayer', false);

const handleAddLayerToListSuccess = (state, layer) => (
  state.updateIn(['layersList'], layersList => layersList.concat({
    ...layer,
    id: generateId(),
  }))
);

const handleAddLayerToListFail = (state, error) => state.set('error', error);


const handleEditLayer = (state, name) => {
  const activeLayerId = state.get('activeLayerId');
  return state.updateIn(['layersList'], layersList =>
    layersList.map((layer) => {
      if (layer.id !== activeLayerId) {
        return layer;
      }
      return {
        ...layer,
        name,
      };
    }));
};


const handleDeleteLayerFromListSuccess = (state, layerId = '') => state
  .updateIn(['layersList'], layersList => layersList.filter(({ id } = {}) => id !== layerId))
  .updateIn(['layersMarkers'], layersMarkers => layersMarkers.remove(layerId.toString()));

const handleDeleteLayerFromListFail = (state, error) => state.set('error', error);


const handleFetchLayersListSuccess = (state, { layersList, activeLayerId }) => state
  .updateIn(['layersList'], array => array.concat(layersList))
  .setIn(['activeLayerId'], activeLayerId);

//
const handleFetchLayersListFail = (state, error) => state.set('error', error);


// Так как все данные уже загружены (моки), вот в этом месте просто сетим актуальный Id
const handleFetchLayerInfo = (state, id) => state.set('activeLayerId', id);

const handleFetchLayerInfoSuccess = state => state;

const handleFetchLayerInfoFail = (state, error) => state.set('error', error);


const handleAddMarker = (state, marker) => {
  const activeLayerId = state.get('activeLayerId');
  return state.updateIn(['layersMarkers', activeLayerId.toString()], (currentMarkers) => {
    if (!currentMarkers) {
      return [marker];
    }
    return currentMarkers.concat(marker);
  });
};

const handleDeleteMarker = (state, marker) => {
  const activeLayerId = state.get('activeLayerId');
  return state.updateIn(['layersMarkers', activeLayerId.toString()], (currentMarkers) => {
    if (!currentMarkers) {
      return [];
    }
    return currentMarkers.filter(currentMarker => !compareMaker(currentMarker, marker));
  });
};

const reducer = createReducer((on) => {
  on(fetchLayersListSuccess, handleFetchLayersListSuccess);
  on(fetchLayersListFail, handleFetchLayersListFail);

  on(fetchLayerInfo, handleFetchLayerInfo);
  on(fetchLayerInfoSuccess, handleFetchLayerInfoSuccess);
  on(fetchLayerInfoFail, handleFetchLayerInfoFail);

  on(openModalAddLayer, handleOpenModalAddLayer);

  on(addLayerToList, handleAddLayerToList);
  on(addLayerToListSuccess, handleAddLayerToListSuccess);
  on(addLayerToListFail, handleAddLayerToListFail);

  on(deleteLayerFromListSuccess, handleDeleteLayerFromListSuccess);
  on(deleteLayerFromListFail, handleDeleteLayerFromListFail);

  on(editLayerName, handleEditLayer);

  on(onSaveLayerSuccess, handleOnSaveLayerSuccess);
  on(onSaveLayerFail, handleOnSaveLayerFail);

  on(addMarker, handleAddMarker);
  on(deleteMarker, handleDeleteMarker);
}, initialState);

export default reducer;
