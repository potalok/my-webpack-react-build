import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import LayersList from './LayersList';
import ModalAddLayer from './ModalAddLayer';
import styles from './LefletGeoMap.scss';

class LefletGeoMap extends Component {
  static propTypes = {
    isOpenAddLayer: PropTypes.bool,
    layersList: PropTypes.array,
    layerMarkers: PropTypes.array,
    onChangeLayer: PropTypes.func,
    onAddMarker: PropTypes.func,
    onDeleteMarker: PropTypes.func,
    onFetchLayersList: PropTypes.func,
    onDeleteLayerFromList: PropTypes.func,
    openModalAddLayer: PropTypes.func,
    onEditLayerName: PropTypes.func,
    addLayerToList: PropTypes.func,
    onSaveLayer: PropTypes.func,
    activeId: PropTypes.string,
  };
  static defaultProps = {
    isOpenAddLayer: false,
    layersList: [],
    layerMarkers: [],
    onChangeLayer: () => {},
    onAddMarker: () => {},
    onDeleteMarker: () => {},
    openModalAddLayer: () => {},
    onDeleteLayerFromList: () => {},
    onEditLayerName: () => {},
    onSaveLayer: () => {},
    activeId: null,
  };


  componentWillMount() {
    this.props.onFetchLayersList();
  }
  handleOpenModalAddLayer = status => () => {
    this.props.openModalAddLayer(status);
  }

    handleClickOnMap = (e = {}) => {
      this.props.onAddMarker(e.latlng);
    }

    handleOnClickMarker = (marker = {}) => () => {
      this.props.onDeleteMarker(marker);
    }

    handleOnSaveChanges = () => {
      const {
        onSaveLayer, layersList, activeId, layerMarkers,
      } = this.props;
      const activeLayer = layersList.find(({ id } = {}) => id === activeId) || {};
      onSaveLayer({
        ...activeLayer,
        layerMarkers,
      });
    }

    renderLayersList = () => {
      const { activeId, layersList } = this.props;
      return (
        <div className={styles.list}>
          <div>
            <button
              type='button'
              className={styles.button}
              onClick={this.handleOpenModalAddLayer(true)}
            >
                  Добавить новую карту
            </button>
            {this.renderModalAddLayer()}
          </div>
          <LayersList
            activeId={activeId}
            onClick={this.props.onChangeLayer}
            layersList={layersList}
            onEditLayer={this.props.onEditLayerName}
            onDelete={this.props.onDeleteLayerFromList}
          />
          <div>
            <button
              type='button'
              className={styles.button}
              onClick={this.handleOnSaveChanges}
            >
                  Сохранить слой
            </button>
          </div>
        </div>
      );
    }


    renderModalAddLayer = () => {
      const { isOpenAddLayer, addLayerToList } = this.props;
      if (!isOpenAddLayer) {
        return null;
      }

      return (
        <ModalAddLayer
          onSaveLayer={addLayerToList}
          onClose={this.handleOpenModalAddLayer(false)}
          open={isOpenAddLayer}
        />
      );
    }


    renderLayerMarkers = () => {
      const { layerMarkers } = this.props;
      return layerMarkers.map((marker = { }) => (
        <Marker
          position={marker}
          key={marker.lat + marker.lng}
          onClick={this.handleOnClickMarker(marker)}
        />
      ));
    }


    render() {
      const { layersList, activeId } = this.props;

      const activeLayer = layersList.find(({ id } = {}) => id === activeId) || {};

      return (
        <div className={styles.main}>
          <div className={styles.layerMap}>

            <p className={styles.layerTitle}>{activeLayer.name}</p>

            <Map
              center={activeLayer.latlng}
              length={4}
              onClick={this.handleClickOnMap}
              zoom={13}
            >
              <TileLayer
                attribution='&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors'
                url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
              />
              {this.renderLayerMarkers()}
            </Map>
          </div>
          {this.renderLayersList()}
        </div>
      );
    }
}

export default LefletGeoMap;
