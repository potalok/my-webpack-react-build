import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from './LayersList.scss';

class LayersList extends PureComponent {
    static propTypes = {
      activeId: PropTypes.oneOf([PropTypes.number, PropTypes.string]),
      layersList: PropTypes.array,
      onDelete: PropTypes.func,
      onClick: PropTypes.func,
      onEditLayer: PropTypes.func,
    };
    static defaultProps = {
      layersList: [],
      onDelete: () => {},
      onClick: () => {},
      onEditLayer: () => {},
    };

    handleOnEditLayer = ({ target = { } } = { }) => {
      this.props.onEditLayer(target.value);
    }

    handleOnClickLayer = layerId => () => {
      this.props.onClick(layerId);
    }

    handleOnDeleteLayer = layerId => (e) => {
      this.props.onDelete(layerId);
      e.stopPropagation();
    }

    renderOneLayer = (layer = {}) => {
      const { activeId } = this.props;
      const { name, id } = layer;

      if (id === activeId) {
        return (
          <li
            key={id}
            className={styles.item}
            onClick={this.handleOnClickLayer(layer.id)}
          >
            <input
              className={styles.listInput}
              value={name}
              onChange={this.handleOnEditLayer}
            />
            <button
              className={styles.listButton}
              type='button'
              onClick={this.handleOnDeleteLayer(layer.id)}
            >
                удалить
            </button>
          </li>
        );
      }
      return (
        <li
          key={id}
          className={styles.item}
          onClick={this.handleOnClickLayer(layer.id)}
        >
          <span className={styles.listText}> {name} </span>
          <button
            className={styles.listButton}
            type='button'
            onClick={this.handleOnDeleteLayer(layer.id)}
          >
                удалить
          </button>
        </li>
      );
    }

    render() {
      const { layersList } = this.props;

      return (
        <ul className={styles.list}>
          {layersList.map(this.renderOneLayer, this)}
        </ul>
      );
    }
}

export default LayersList;
