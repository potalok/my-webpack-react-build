import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Map, TileLayer } from 'react-leaflet';
import ReactModal from 'react-modal';
import styles from './ModalAddLayer.scss';


const customStyles = {
  overlay: {
    zIndex: 1000,
  },
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    width: '80%',
    zIndex: 100,
    padding: 0,
    transform: 'translate(-50%, -50%)',
  },
};

class ModalAddLayer extends Component {
    static propTypes = {
      open: PropTypes.bool,
      onClose: PropTypes.func,
      onSaveLayer: PropTypes.func,
    };
    static defaultProps = {
      onSaveLayer: () => {},
      onClose: () => {},
      open: false,
    };

    state = {
      latlng: {
        lat: 55.75086519256549,
        lng: 37.61816185787291,
      },
      layerName: '',
    };


    handleClickOnMap = (e = {}) => {
      this.setState({
        latlng: e.latlng,
      });
    }


    handleCloseModal = () => {
      this.props.onClose();
    }

    handleOnChangeNameLayer = ({ target = {} } = {}) => {
      this.setState({
        layerName: target.value,
      });
    }

    handleSaveLayer = () => {
      const { layerName, latlng } = this.state;
      this.props.onSaveLayer({
        name: layerName,
        latlng,
      });
    }


    renderContent = () => {
      const { layerName = '' } = this.state;
      return (
        <div className={styles.content}>
          <button
            className={styles.button}
            onClick={this.handleCloseModal}
          >
                Закрыть
          </button>

          {this.renderNameInput()}

          <button
            className={styles.button}
            onClick={this.handleSaveLayer}
            disabled={layerName.trim() === ''}
          >
                 Сохранить слой
          </button>
          <span className={styles.textInfo}>
                 Нажатием на карту вы можете установить центр позиционирования новой карты
          </span>
        </div>
      );
    }


    renderNameInput = () => (
      <div className={styles.field}>
        <label className={styles.label}>
                Введите имя карты
                <input
                  className={styles.input}
                  onChange={this.handleOnChangeNameLayer}
                  value={this.state.layerName}
                />
        </label>

      </div>
    )

    render() {
      const { open } = this.props;
      const { layerName = '' } = this.state;
      return (
        <div>
          <ReactModal
            isOpen={open}
            contentLabel='onRequestClose Example'
            onRequestClose={this.handleCloseModal}
            style={customStyles}
          >
            <div className={styles.container}>
              <div className={styles.map}>
                <p className={styles.layerTitle}>{layerName}</p>
                <Map
                  center={this.state.latlng}
                  length={4}
                  onClick={this.handleClickOnMap}
                  zoom={13}
                >
                  <TileLayer
                    attribution='&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors'
                    url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
                  />
                </Map>
              </div>
              {this.renderContent()}
            </div>

          </ReactModal>
        </div>
      );
    }
}

export default ModalAddLayer;
